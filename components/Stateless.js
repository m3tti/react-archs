import React from 'react'

const inc = x => x + 1;
const dec = x => x - 1;

export const Stateless = ({ value, update }) => 
    <div>
    <h1>{value}</h1>
    <button onClick={() => update(inc)}>Inc</button>
    <button onClick={() => update(dec)}>Dec</button>
    </div>
