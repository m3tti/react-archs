import React, { Component } from 'react'

const inc = (state, props) => ({ value: state.value + 1 });
const dec = (state, props) => ({ value: state.value - 1 });

export class FunctionalStatefull extends Component {
    constructor(props) {
        super(props)
        this.state = {value: parseInt(props.value)}
    }

    render() {
        return <div>
            <h1>{this.state.value}</h1>
            <button onClick={() => this.setState(inc)}>Inc</button>
            <button onClick={() => this.setState(dec)}>Dec</button>
            </div>
    }
}
