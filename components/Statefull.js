import React, { Component } from 'react'

export class Statefull extends Component {
    constructor(props) {
        super(props)
        this.state = {value: parseInt(props.value)}
    }

    inc() {
        this.setState({ value: this.state.value + 1});
    }

    dec() {
        this.setState({ value: this.state.value - 1});
    }
    
    render() {
        return <div>
            <h1>{this.state.value}</h1>
            <button onClick={() => this.inc()}>Inc</button>
            <button onClick={() => this.dec()}>Dec</button>
            </div>
    }
}
