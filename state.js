import React from 'react'
import cards from 'reactcards'
import { Statefull } from './components/Statefull'
import { FunctionalStatefull } from './components/FunctionalStatefull'
import { Stateless } from './components/Stateless'

const demo = cards('demo')

demo.card(
        <Statefull value="22"/>
)

demo.card(
        <FunctionalStatefull value="22"/>
)

demo.card(
    state => <Stateless value={state.get()} update={state.update}/>,
    { init: 22 }
)
