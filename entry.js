import {run} from 'reactcards';
import './state';


if (module.hot) {
    module.hot.accept()
}

// off we go...
run();
